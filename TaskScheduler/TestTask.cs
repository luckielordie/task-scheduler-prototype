﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskScheduler
{
    public class TestTask : Task
    {
        public TestTask(string taskID, TaskData taskData, List<Task> childTasks = null, Task taskDependency = null, TaskPriority taskPriority = TaskPriority.Low)
            : base(taskID, taskData, childTasks, taskDependency, taskPriority)
        {

        }

        public override void Execute()
        {
            Console.WriteLine("[TASK]");
        }
    }
}
