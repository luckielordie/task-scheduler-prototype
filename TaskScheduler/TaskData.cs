﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskScheduler
{
    public enum TaskPriority
    {
        Critical,
        VeryHigh,
        High,
        Medium,
        Low
    }

    public class TaskData
    {
        private TaskPriority m_taskPriority;
    }
}
