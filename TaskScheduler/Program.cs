﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskScheduler
{
    class Program
    {
        static void Main(string[] args)
        {
            TaskManager tm = new TaskManager();
            tm.AddTask(new TestTask("test", null));
            while(true)
            {
                Console.Clear();
                Console.WriteLine(tm.ToString());
            }
        }
    }
}
