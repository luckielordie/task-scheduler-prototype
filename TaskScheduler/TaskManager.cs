﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace TaskScheduler
{
    public class TaskManager
    {
        private List<Thread> m_workerThreads;
        private ConcurrentQueue<Task> m_currentTasks;
        private object m_locker  = new object();

        public TaskManager()
        {
            m_workerThreads = new List<Thread>();
            
            for (int i = 0; i < 4; i++)
            {
                Thread worker = new Thread(Consume) { IsBackground = true, Name = string.Format("Worker Thread {0}", i) };
                m_workerThreads.Add(worker);
                worker.Start();
            }
                
            m_currentTasks = new ConcurrentQueue<Task>();
        }

        public void AddTask(Task newTask)
        {
            m_currentTasks.Enqueue(newTask);
        }

        void Consume()
        {
            while (true)
            {
                Task task;
                lock (m_locker)
                {
                    while(m_currentTasks.TryDequeue(out task) != true);
                }

                task.Execute();
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("TaskManager: ");
            
            foreach(Thread worker in m_workerThreads)
            {
                sb.AppendLine("--[WORKER] Name: " + worker.Name + " , State: " + worker.ThreadState);
            }

            return sb.ToString();
        }
    }
}
